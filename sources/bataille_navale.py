"""
Créé grille vide et affiche avec les fonctions dessous
"""
from random import randint
#fonctions créations nouvelles grilles

def grille_vide_user():
    """
    Créé une nouvelle grille pour l'utilisateur et la retourne
    """
    grille_user=[[" ","A","B","C","D","E","F","G","H","I","J"],["1"," "," "," "," "," "," "," "," "," "," "],["2"," "," "," "," "," "," "," "," "," "," "],["3"," "," "," "," "," "," "," "," "," "," "],["4"," "," "," "," "," "," "," "," "," "," "],["5"," "," "," "," "," "," "," "," "," "," "],["6"," "," "," "," "," "," "," "," "," "," "],["7"," "," "," "," "," "," "," "," "," "," "],["8"," "," "," "," "," "," "," "," "," "," "],["9"," "," "," "," "," "," "," "," "," "," "],["X"," "," "," "," "," "," "," "," "," "," "]]
    return grille_user

def grille_vide_ai():
    """
    fonctions créé une nouvelle grille pour l'IA et la retourne
    """
    grille_ai=[[" ","A","B","C","D","E","F","G","H","I","J"],["1"," "," "," "," "," "," "," "," "," "," "],["2"," "," "," "," "," "," "," "," "," "," "],["3"," "," "," "," "," "," "," "," "," "," "],["4"," "," "," "," "," "," "," "," "," "," "],["5"," "," "," "," "," "," "," "," "," "," "],["6"," "," "," "," "," "," "," "," "," "," "],["7"," "," "," "," "," "," "," "," "," "," "],["8"," "," "," "," "," "," "," "," "," "," "],["9"," "," "," "," "," "," "," "," "," "," "],["X"," "," "," "," "," "," "," "," "," "," "]]
    return grille_ai
def grille_vide_ai_shown():
    """
    fonctions créé une nouvelle grille
    """
    grille_ai_shown=[[" ","A","B","C","D","E","F","G","H","I","J"],["1"," "," "," "," "," "," "," "," "," "," "],["2"," "," "," "," "," "," "," "," "," "," "],["3"," "," "," "," "," "," "," "," "," "," "],["4"," "," "," "," "," "," "," "," "," "," "],["5"," "," "," "," "," "," "," "," "," "," "],["6"," "," "," "," "," "," "," "," "," "," "],["7"," "," "," "," "," "," "," "," "," "," "],["8"," "," "," "," "," "," "," "," "," "," "],["9"," "," "," "," "," "," "," "," "," "," "],["X"," "," "," "," "," "," "," "," "," "," "]]
    return grille_ai_shown
#Affichage
def affichage_ai(grille_ai):
    """
    Affiche la grille de jeu de l'IA
    """
    print("  ",grille_ai[0][1]," ",grille_ai[0][2]," ",grille_ai[0][3]," ",grille_ai[0][4]," ",grille_ai[0][5]," ",grille_ai[0][6]," ",grille_ai[0][7]," ",grille_ai[0][8]," ",grille_ai[0][9]," ",grille_ai[0][10],
    "\n ┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐",
    "\n1│",grille_ai[1][1],"│",grille_ai[1][2],"│",grille_ai[1][3],"│",grille_ai[1][4],"│",grille_ai[1][5],"│",grille_ai[1][6],"│",grille_ai[1][7],"│",grille_ai[1][8],"│",grille_ai[1][9],"│",grille_ai[1][10],"│",
    "\n ├───┼───┼───┼───┼───┼───┼───┼───┼───┴───┤",
    "\n2│",grille_ai[2][1],"│",grille_ai[2][2],"│",grille_ai[2][3],"│",grille_ai[2][4],"│",grille_ai[2][5],"│",grille_ai[2][6],"│",grille_ai[2][7],"│",grille_ai[2][8],"│",grille_ai[2][9],"│",grille_ai[2][10],"│",
    "\n ├───┼───┼───┼───┼───┼───┼───┼───┼───┼───┤",
    "\n3│",grille_ai[3][1],"│",grille_ai[3][2],"│",grille_ai[3][3],"│",grille_ai[3][4],"│",grille_ai[3][5],"│",grille_ai[3][6],"│",grille_ai[3][7],"│",grille_ai[3][8],"│",grille_ai[3][9],"│",grille_ai[3][10],"│",
    "\n ├───┼───┼───┼───┼───┼───┼───┼───┼───┼───┤",
    "\n4│",grille_ai[4][1],"│",grille_ai[4][2],"│",grille_ai[4][3],"│",grille_ai[4][4],"│",grille_ai[4][5],"│",grille_ai[4][6],"│",grille_ai[4][7],"│",grille_ai[4][8],"│",grille_ai[4][9],"│",grille_ai[4][10],"│",
    "\n ├───┼───┼───┼───┼───┼───┼───┼───┼───┼───┤",
    "\n5│",grille_ai[5][1],"│",grille_ai[5][2],"│",grille_ai[5][3],"│",grille_ai[5][4],"│",grille_ai[5][5],"│",grille_ai[5][6],"│",grille_ai[5][7],"│",grille_ai[5][8],"│",grille_ai[5][9],"│",grille_ai[5][10],"│",
    "\n ├───┼───┼───┼───┼───┼───┼───┼───┼───┼───┤",
    "\n6│",grille_ai[6][1],"│",grille_ai[6][2],"│",grille_ai[6][3],"│",grille_ai[6][4],"│",grille_ai[6][5],"│",grille_ai[6][6],"│",grille_ai[6][7],"│",grille_ai[6][8],"│",grille_ai[6][9],"│",grille_ai[6][10],"│",
    "\n ├───┼───┼───┼───┼───┼───┼───┼───┼───┼───┤",
    "\n7│",grille_ai[7][1],"│",grille_ai[7][2],"│",grille_ai[7][3],"│",grille_ai[7][4],"│",grille_ai[7][5],"│",grille_ai[7][6],"│",grille_ai[7][7],"│",grille_ai[7][8],"│",grille_ai[7][9],"│",grille_ai[7][10],"│",
    "\n ├───┼───┼───┼───┼───┼───┼───┼───┼───┼───┤",
    "\n8│",grille_ai[8][1],"│",grille_ai[8][2],"│",grille_ai[8][3],"│",grille_ai[8][4],"│",grille_ai[8][5],"│",grille_ai[8][6],"│",grille_ai[8][7],"│",grille_ai[8][8],"│",grille_ai[8][9],"│",grille_ai[8][10],"│",
    "\n ├───┼───┼───┼───┼───┼───┼───┼───┼───┼───┤",
    "\n9│",grille_ai[9][1],"│",grille_ai[9][2],"│",grille_ai[9][3],"│",grille_ai[9][4],"│",grille_ai[9][5],"│",grille_ai[9][6],"│",grille_ai[9][7],"│",grille_ai[9][8],"│",grille_ai[9][9],"│",grille_ai[9][10],"│",
    "\n ├───┼───┼───┼───┼───┼───┼───┼───┼───┼───┤",
    "\nX│",grille_ai[10][1],"│",grille_ai[10][2],"│",grille_ai[10][3],"│",grille_ai[10][4],"│",grille_ai[10][5],"│",grille_ai[10][6],"│",grille_ai[10][7],"│",grille_ai[10][8],"│",grille_ai[10][9],"│",grille_ai[10][10],"│",
    "\n └───┴───┴───┴───┴───┴───┴───┴───┴───┴───┘")

def affichage_user(grille_user):
    """
    Affiche la grille de l'utilisateur
    """
    print("  ",grille_user[0][1]," ",grille_user[0][2]," ",grille_user[0][3]," ",grille_user[0][4]," ",grille_user[0][5]," ",grille_user[0][6]," ",grille_user[0][7]," ",grille_user[0][8]," ",grille_user[0][9]," ",grille_user[0][10],
    "\n ┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐",
    "\n1│",grille_user[1][1],"│",grille_user[1][2],"│",grille_user[1][3],"│",grille_user[1][4],"│",grille_user[1][5],"│",grille_user[1][6],"│",grille_user[1][7],"│",grille_user[1][8],"│",grille_user[1][9],"│",grille_user[1][10],"│",
    "\n ├───┼───┼───┼───┼───┼───┼───┼───┼───┴───┤",
    "\n2│",grille_user[2][1],"│",grille_user[2][2],"│",grille_user[2][3],"│",grille_user[2][4],"│",grille_user[2][5],"│",grille_user[2][6],"│",grille_user[2][7],"│",grille_user[2][8],"│",grille_user[2][9],"│",grille_user[2][10],"│",
    "\n ├───┼───┼───┼───┼───┼───┼───┼───┼───┼───┤",
    "\n3│",grille_user[3][1],"│",grille_user[3][2],"│",grille_user[3][3],"│",grille_user[3][4],"│",grille_user[3][5],"│",grille_user[3][6],"│",grille_user[3][7],"│",grille_user[3][8],"│",grille_user[3][9],"│",grille_user[3][10],"│",
    "\n ├───┼───┼───┼───┼───┼───┼───┼───┼───┼───┤",
    "\n4│",grille_user[4][1],"│",grille_user[4][2],"│",grille_user[4][3],"│",grille_user[4][4],"│",grille_user[4][5],"│",grille_user[4][6],"│",grille_user[4][7],"│",grille_user[4][8],"│",grille_user[4][9],"│",grille_user[4][10],"│",
    "\n ├───┼───┼───┼───┼───┼───┼───┼───┼───┼───┤",
    "\n5│",grille_user[5][1],"│",grille_user[5][2],"│",grille_user[5][3],"│",grille_user[5][4],"│",grille_user[5][5],"│",grille_user[5][6],"│",grille_user[5][7],"│",grille_user[5][8],"│",grille_user[5][9],"│",grille_user[5][10],"│",
    "\n ├───┼───┼───┼───┼───┼───┼───┼───┼───┼───┤",
    "\n6│",grille_user[6][1],"│",grille_user[6][2],"│",grille_user[6][3],"│",grille_user[6][4],"│",grille_user[6][5],"│",grille_user[6][6],"│",grille_user[6][7],"│",grille_user[6][8],"│",grille_user[6][9],"│",grille_user[6][10],"│",
    "\n ├───┼───┼───┼───┼───┼───┼───┼───┼───┼───┤",
    "\n7│",grille_user[7][1],"│",grille_user[7][2],"│",grille_user[7][3],"│",grille_user[7][4],"│",grille_user[7][5],"│",grille_user[7][6],"│",grille_user[7][7],"│",grille_user[7][8],"│",grille_user[7][9],"│",grille_user[7][10],"│",
    "\n ├───┼───┼───┼───┼───┼───┼───┼───┼───┼───┤",
    "\n8│",grille_user[8][1],"│",grille_user[8][2],"│",grille_user[8][3],"│",grille_user[8][4],"│",grille_user[8][5],"│",grille_user[8][6],"│",grille_user[8][7],"│",grille_user[8][8],"│",grille_user[8][9],"│",grille_user[8][10],"│",
    "\n ├───┼───┼───┼───┼───┼───┼───┼───┼───┼───┤",
    "\n9│",grille_user[9][1],"│",grille_user[9][2],"│",grille_user[9][3],"│",grille_user[9][4],"│",grille_user[9][5],"│",grille_user[9][6],"│",grille_user[9][7],"│",grille_user[9][8],"│",grille_user[9][9],"│",grille_user[9][10],"│",
    "\n ├───┼───┼───┼───┼───┼───┼───┼───┼───┼───┤",
    "\nX│",grille_user[10][1],"│",grille_user[10][2],"│",grille_user[10][3],"│",grille_user[10][4],"│",grille_user[10][5],"│",grille_user[10][6],"│",grille_user[10][7],"│",grille_user[10][8],"│",grille_user[10][9],"│",grille_user[10][10],"│",
    "\n └───┴───┴───┴───┴───┴───┴───┴───┴───┴───┘")

def affichage_ai_test(grille_ai):
    print("  ",grille_ai[0][1]," ",grille_ai[0][2]," ",grille_ai[0][3]," ",grille_ai[0][4]," ",grille_ai[0][5]," ",grille_ai[0][6]," ",grille_ai[0][7]," ",grille_ai[0][8]," ",grille_ai[0][9]," ",grille_ai[0][10],
    "\n ┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┐",
    "\n1│",grille_ai[1][1],"│",grille_ai[1][2],"│",grille_ai[1][3],"│",grille_ai[1][4],"│",grille_ai[1][5],"│",grille_ai[1][6],"│",grille_ai[1][7],"│",grille_ai[1][8],"│",grille_ai[1][9],"│",grille_ai[1][10],"│",
    "\n ├───┼───┼───┼───┼───┼───┼───┼───┼───┴───┤",
    "\n2│",grille_ai[2][1],"│",grille_ai[2][2],"│",grille_ai[2][3],"│",grille_ai[2][4],"│",grille_ai[2][5],"│",grille_ai[2][6],"│",grille_ai[2][7],"│",grille_ai[2][8],"│",grille_ai[2][9],"│",grille_ai[2][10],"│",
    "\n ├───┼───┼───┼───┼───┼───┼───┼───┼───┼───┤",
    "\n3│",grille_ai[3][1],"│",grille_ai[3][2],"│",grille_ai[3][3],"│",grille_ai[3][4],"│",grille_ai[3][5],"│",grille_ai[3][6],"│",grille_ai[3][7],"│",grille_ai[3][8],"│",grille_ai[3][9],"│",grille_ai[3][10],"│",
    "\n ├───┼───┼───┼───┼───┼───┼───┼───┼───┼───┤",
    "\n4│",grille_ai[4][1],"│",grille_ai[4][2],"│",grille_ai[4][3],"│",grille_ai[4][4],"│",grille_ai[4][5],"│",grille_ai[4][6],"│",grille_ai[4][7],"│",grille_ai[4][8],"│",grille_ai[4][9],"│",grille_ai[4][10],"│",
    "\n ├───┼───┼───┼───┼───┼───┼───┼───┼───┼───┤",
    "\n5│",grille_ai[5][1],"│",grille_ai[5][2],"│",grille_ai[5][3],"│",grille_ai[5][4],"│",grille_ai[5][5],"│",grille_ai[5][6],"│",grille_ai[5][7],"│",grille_ai[5][8],"│",grille_ai[5][9],"│",grille_ai[5][10],"│",
    "\n ├───┼───┼───┼───┼───┼───┼───┼───┼───┼───┤",
    "\n6│",grille_ai[6][1],"│",grille_ai[6][2],"│",grille_ai[6][3],"│",grille_ai[6][4],"│",grille_ai[6][5],"│",grille_ai[6][6],"│",grille_ai[6][7],"│",grille_ai[6][8],"│",grille_ai[6][9],"│",grille_ai[6][10],"│",
    "\n ├───┼───┼───┼───┼───┼───┼───┼───┼───┼───┤",
    "\n7│",grille_ai[7][1],"│",grille_ai[7][2],"│",grille_ai[7][3],"│",grille_ai[7][4],"│",grille_ai[7][5],"│",grille_ai[7][6],"│",grille_ai[7][7],"│",grille_ai[7][8],"│",grille_ai[7][9],"│",grille_ai[7][10],"│",
    "\n ├───┼───┼───┼───┼───┼───┼───┼───┼───┼───┤",
    "\n8│",grille_ai[8][1],"│",grille_ai[8][2],"│",grille_ai[8][3],"│",grille_ai[8][4],"│",grille_ai[8][5],"│",grille_ai[8][6],"│",grille_ai[8][7],"│",grille_ai[8][8],"│",grille_ai[8][9],"│",grille_ai[8][10],"│",
    "\n ├───┼───┼───┼───┼───┼───┼───┼───┼───┼───┤",
    "\n9│",grille_ai[9][1],"│",grille_ai[9][2],"│",grille_ai[9][3],"│",grille_ai[9][4],"│",grille_ai[9][5],"│",grille_ai[9][6],"│",grille_ai[9][7],"│",grille_ai[9][8],"│",grille_ai[9][9],"│",grille_ai[9][10],"│",
    "\n ├───┼───┼───┼───┼───┼───┼───┼───┼───┼───┤",
    "\nX│",grille_ai[10][1],"│",grille_ai[10][2],"│",grille_ai[10][3],"│",grille_ai[10][4],"│",grille_ai[10][5],"│",grille_ai[10][6],"│",grille_ai[10][7],"│",grille_ai[10][8],"│",grille_ai[10][9],"│",grille_ai[10][10],"│",
    "\n └───┴───┴───┴───┴───┴───┴───┴───┴───┴───┘")


def name_bot():
    """
    Donne un nom au bot
    """
    prenoms = ["Cheese", "Icecream", "Gun", "Pie", "Axe", "Nobody", "Ananas", "Blueberry", "Pancake", "Red", "Cookie", "Boo"]
    a = randint(0, len(prenoms)-1)
    name = prenoms[a]
    print("je m'appelle",name)

def bateau(nb_cases, x, y, how, grille_user):
    """
    Placement des bateaux sur la grille
    Donc, j'ai beaucoup compléxifié la fonction, musers en gros ça va simuler avant la position du bateau pour savoir si celle ci est possible puis renvoyer un erreur ou placer le bateau
    Args:
        nb_cases (int): nombre de cases du bateau
        x (int): colonne de départ
        y (int): ligne de départ
        how (int): sens du bateau (1 pour vertical, 2 pour horizontal, 3 pour diagonal)
        grille_user (list): liste représentant la grille de jeu
    Returns:
        grille_user (list): liste représentant la grille de jeu avec le bateau placé
        erreur_placement_boat_2 (bool): True si une erreur de placement est détectée, False sinon
    """
    erreur_placement_boat_2 = False
    lettres=["e","d","c","b","a"]
    if how == 1: #Vertical
        for i in range(nb_cases):
            if y+i >= len(grille_user) or grille_user[y+i][x] !=' ':
                erreur_placement_boat_2 = True

        if erreur_placement_boat_2 == False:
            for i in range(nb_cases):
                grille_user[y+i][x] = lettres[nb_cases-1]

    elif how == 2: #Horizontal
        for i in range(nb_cases):
            if x+i >= len(grille_user[0]) or grille_user[y+i][x] !=' ':
                erreur_placement_boat_2 = True

        if erreur_placement_boat_2 ==False:
            for i in range(nb_cases):
                grille_user[y+i][x] = lettres[nb_cases-1]

    elif how == 3: #Diagonal
        for i in range(nb_cases):
            if x+i >= 10 or y+i >= len(grille_user) or grille_user[y+i][x+i] !=' ':
                erreur_placement_boat_2 = True

        if erreur_placement_boat_2 ==False:
            for i in range(nb_cases):
                grille_user[y+i][x+i] = lettres[nb_cases-1]
    return grille_user,erreur_placement_boat_2

def moins_quota (quota,erreur_placement_boat_2):
    """
    Déduit le nombre de bateaux de la taille donnée du quota disponible.

    Paramètres:
        quota (List[int]) : Le quota disponible pour l'utilisateur.
        erreur_placement_boat_2 (bool) : Indique si l'utilisateur a fait une erreur en signalant le deuxième bateau.

    Retour:
        List[int] : Le quota disponible mis à jour pour l'utilisateur.
    """
    if erreur_placement_boat_2==False:
        if nb_cases==5:
            quota[0]-=1
        elif nb_cases==4:
            quota[1]-=1
        elif nb_cases==3:
            quota[2]-=1
        elif nb_cases==2:
            quota[3]-=1
    return quota


def placer_bateau (quota):
    """
    Demande à l'utilisateur la position de ses bateaux
    Elle vient avant bateau
    Args:
        quota (list): liste des quotas des bateaux disponibles
    Returns:
        tuple: la position du bateau (y, x, sens), et un booléen indiquant si une erreur de placement s'est produite
    """


    nb_cases = int(input("De combien de cases est le bateau que tu souhaites placer ? (5 ou 4 ou 3 ou 2)"))
    while nb_cases != 5 and nb_cases != 3 and nb_cases != 2 and nb_cases!=4:
            print("Erreur, Réessaie")
            nb_cases =  int(input("De combien de cases est le bateau que tu souhaites placer ? (5 ou 4 ou 3 ou 2)"))

    while (quota[0]==0 and nb_cases==5) or (quota[1]==0 and nb_cases==4) or (quota[2]==0 and nb_cases==3) or (quota[3]==0 and nb_cases==2):
            print("Erreur, Réessaie")
            nb_cases =  int(input("De combien de cases est le bateau que tu souhaites placer ? (5 ou 4 ou 3 ou 2)"))

    y = int(input("Où tu veux placer ton bateau sur axe des y: "))
    while y <1 or y > 10:
        print("Erreur, Réessaie")
        y = int(input("Où tu veux placer ton bateau sur axe des y: "))
    x = input("Où tu veux placer ton bateau sur axe des x: ")
    if x == 'A' or x == 'a':
        x = 1
    elif x == 'B' or x == 'b':
        x = 2
    elif x == 'C' or x == 'c':
        x = 3
    if x == 'D' or x == 'd':
        x = 4
    if x == 'E' or x == 'e':
        x = 5
    if x == 'F' or x == 'f':
        x = 6
    if x == 'G' or x == 'g':
        x = 7
    if x == 'H' or x == 'h':
        x = 8
    if x == 'i' or x == 'i':
        x = 9
    if x == 'j' or x == 'j':
        x = 10
    while x <1 or x > 10:
        print("Erreur, Réessaie")
        x = int(input("Où tu veux placer ton bateau sur axe des x: "))

    how = int(input("Comment doit etre le bateau: (Vertical[1] ou horizontal[2] ou diagonal[3])"))
    while how <1 or how > 3:
        print("Erreur, Réessaie")
        how = int(input("Comment doit etre le bateau: (Vertical[1] ou horizontal[2] ou diagonal[3])"))
    return how,nb_cases,y,x

def bateau_ai(nb_cases, x, y, how, grille_ai):
    """
    Placement des bateaux sur la grille
    Donc, j'ai beaucoup compléxifié la fonction, musers en gros ça va simuler avant la position du bateau pour savoir si celle ci est possible puis renvoyer un erreur ou placer le bateau
    Args:
        nb_cases (int): nombre de cases du bateau
        x (int): colonne de départ
        y (int): ligne de départ
        how (int): sens du bateau (1 pour vertical, 2 pour horizontal, 3 pour diagonal)
        grille_ai (list): liste représentant la grille de jeu
    Returns:
        grille_ai (list): liste représentant la grille de jeu avec le bateau placé
        erreur_placement_boat_2 (bool): True si une erreur de placement est détectée, False sinon
    """
    erreur_placement_boat_2 = False
    lettres=["e","d","c","b","a"]
    if how == 1: #Vertical
        for i in range(nb_cases):
            if y+i >= len(grille_ai) or grille_ai[y+i][x] !=' ':
                erreur_placement_boat_2 = True

        if erreur_placement_boat_2 == False:
            for i in range(nb_cases):
                grille_ai[y+i][x] = lettres[nb_cases-1]

    elif how == 2: #Horizontal
        for i in range(nb_cases):
            if x+i >= len(grille_ai[0]) or grille_ai[y][x+i] !=' ':
                erreur_placement_boat_2 = True

        if erreur_placement_boat_2 ==False:
            for i in range(nb_cases):
                grille_ai[y+i][x] = lettres[nb_cases-1]

    elif how == 3: #Diagonal
        for i in range(nb_cases):
            if x+i >= 10 or y+i >= len(grille_ai) or grille_ai[y+i][x+i] !=' ':
                erreur_placement_boat_2 = True

        if erreur_placement_boat_2 ==False:
            for i in range(nb_cases):
                grille_ai[y+i][x+i] = lettres[nb_cases-1]
    return grille_ai,erreur_placement_boat_2





def placer_bateau_ai (quota_ai):
    """
    Demande à l'IA la position de ses bateaux
    Elle vient avant bateau
    Args :
        quota_ai (List[int]) : Le quota disponible pour l'IA.
    Retour:
        tuple : la position du bateau (y, x, sens), et un booléen indiquant si une erreur de placement s'est produite
    """

    nb_cases = randint(2,5)
    #while nb_cases != 5 and nb_cases != 3 and nb_cases != 2 and nb_cases!=4:

     #       nb_cases =  randint(2,5)
    #quota_ai.sort()
    while (quota_ai[0]==0 and nb_cases==5) or (quota_ai[1]==0 and nb_cases==4) or (quota_ai[2]==0 and nb_cases==3) or (quota_ai[3]==0 and nb_cases==2):

            nb_cases =  randint(2,5)

    y = randint(1,10)
    while y <1 or y > 10:
        y = randint(1,10)
    x = randint(1,10)
    while x <1 or x > 10:

        x = randint(1,10)

    how = randint(1,3)
    while how <1 or how > 3:

        how = randint(1,3)
    return how,nb_cases,y,x

def moins_quota_ai (quota_ai,erreur_placement_boat_ai_2):
    """
    Déduit le nombre de bateaux de la taille donnée du quota disponible.

    Paramètres:
        quota_ai (List[int]) : Le quota disponible pour l'IA.
        erreur_placement_boat_ai_2 (bool) : Indique si l'IA a fait une erreur en plaçant le deuxième bateau.

    Retour:
        List[int] : le quota disponible mis à jour pour l’IA.
    """
    if erreur_placement_boat_ai_2==False:
        if nb_cases==5:
            quota_ai[0]-=1
        elif nb_cases==4:
            quota_ai[1]-=1
        elif nb_cases==3:
            quota_ai[2]-=1
        elif nb_cases==2:
            quota_ai[3]-=1
    return quota_ai

#verif de touche
def target_hit_on_ai(grille_ai):
    """
    Demande à l'utilisateur de sélectionner une colonne et une ligne sur lesquelles tirer.

    Paramètres:
        grille_ai : Le plateau de jeu actuel.

    Retour:
        bool : indique si la cible sélectionnée a été touchée ou non.
    """
    x = input("Sur quel colonne voulez vous tirez?")
    if x == 'A' or x == 'a':
        x = 1
    if x == 'B' or x == 'b':
        x = 2
    if x == 'C' or x == 'c':
        x = 3
    if x == 'D' or x == 'd':
        x = 4
    if x == 'E' or x == 'e':
        x = 5
    if x == 'F' or x == 'f':
        x = 6
    if x == 'G' or x == 'g':
        x = 7
    if x == 'H' or x == 'h':
        x = 8
    if x == 'i' or x == 'i':
        x = 9
    if x == 'j' or x == 'j':
        x = 10
    y = int(input("Sur quel ligne voulez vous tirer?"))
    if grille_ai[y][x] == " " :
        grille_ai_shown[y][x] = "o"
        target_hit = False
        return target_hit
    elif grille_ai_shown[y][x] == "x" or grille_ai_shown[y][x] == "o" :
        print("vous avez deja choisi cette case")
        target_hit = True
        return target_hit
    else:
        grille_ai[y][x] = "x"
        grille_ai_shown[y][x]="x"
        target_hit = True
    return target_hit

def victoire(grille_ai, grille_user): #Donne la victoire à un joueur et arrête la partie
    """
    Renvoie un tuple contenant un booléen indiquant si le joueur IA a gagné et un booléen indiquant si la partie est terminée.

    Paramètres:
        grille_ai (liste) : Le plateau de jeu du joueur IA.
        grille_user (liste) : Le plateau de jeu du joueur utilisateur.

    Retour:
        tuple : Un tuple contenant un booléen indiquant si le joueur IA a gagné, et un booléen indiquant si la partie est terminée.
    """
    if grille_ai[1].count("a")==0 and grille_ai[2].count("a")==0 and grille_ai[3].count("a")==0 and grille_ai[3].count("a")==0 and grille_ai[4].count("a")==0 and grille_ai[5].count("a")==0 and grille_ai[6].count("a")==0 and grille_ai[7].count("a")==0 and grille_ai[8].count("a")==0 and grille_ai[9].count("a")==0 and grille_ai[10].count("a")==0 and grille_ai[1].count("b")==0 and grille_ai[2].count("b")==0 and grille_ai[3].count("b")==0 and grille_ai[3].count("b")==0 and grille_ai[4].count("b")==0 and grille_ai[5].count("b")==0 and grille_ai[6].count("b")==0 and grille_ai[7].count("b")==0 and grille_ai[8].count("b")==0 and grille_ai[9].count("b")==0 and grille_ai[10].count("b")==0 and grille_ai[1].count("c")==0 and grille_ai[2].count("c")==0 and grille_ai[3].count("c")==0 and grille_ai[3].count("c")==0 and grille_ai[4].count("c")==0 and grille_ai[5].count("c")==0 and grille_ai[6].count("c")==0 and grille_ai[7].count("c")==0 and grille_ai[8].count("c")==0 and grille_ai[9].count("c")==0 and grille_ai[10].count("c")==0 and grille_ai[1].count("d")==0 and grille_ai[2].count("d")==0 and grille_ai[3].count("d")==0 and grille_ai[3].count("d")==0 and grille_ai[4].count("d")==0 and grille_ai[5].count("d")==0 and grille_ai[6].count("d")==0 and grille_ai[7].count("d")==0 and grille_ai[8].count("d")==0 and grille_ai[9].count("d")==0 and grille_ai[10].count("d")==0:
        victoire_a = True
        fin = True
    elif grille_user[1].count("a")==0 and grille_user[2].count("a")==0 and grille_user[3].count("a")==0 and grille_user[3].count("a")==0 and grille_user[4].count("a")==0 and grille_user[5].count("a")==0 and grille_user[6].count("a")==0 and grille_user[7].count("a")==0 and grille_user[8].count("a")==0 and grille_user[9].count("a")==0 and grille_user[10].count("a")==0 and grille_user[1].count("b")==0 and grille_user[2].count("b")==0 and grille_user[3].count("b")==0 and grille_user[3].count("b")==0 and grille_user[4].count("b")==0 and grille_user[5].count("b")==0 and grille_user[6].count("b")==0 and grille_user[7].count("b")==0 and grille_user[8].count("b")==0 and grille_user[9].count("b")==0 and grille_user[10].count("b")==0 and grille_user[1].count("c")==0 and grille_user[2].count("c")==0 and grille_user[3].count("c")==0 and grille_user[3].count("c")==0 and grille_user[4].count("c")==0 and grille_user[5].count("c")==0 and grille_user[6].count("c")==0 and grille_user[7].count("c")==0 and grille_user[8].count("c")==0 and grille_user[9].count("c")==0 and grille_user[10].count("c")==0 and grille_user[1].count("d")==0 and grille_user[2].count("d")==0 and grille_user[3].count("d")==0 and grille_user[3].count("d")==0 and grille_user[4].count("d")==0 and grille_user[5].count("d")==0 and grille_user[6].count("d")==0 and grille_user[7].count("d")==0 and grille_user[8].count("d")==0 and grille_user[9].count("d")==0 and grille_user[10].count("d")==0:
        victoire_a = False
        fin = True
    else:
        victoire_a = False
        fin = False
    return victoire_a, fin


def target_hit_on_user(grille_user,x,y,changetirmode,countermodetir):
    """
    Vérifie si la cible sélectionnée a été touchée.
    Paramètres:
        grille_user (List[List[str]]) : Le plateau de jeu actuel.

    Retour:
        target_hit (bool) : Indique si la cible sélectionnée a été touchée ou non.
    """
    if countermodetir<8:
        changetirmode=True
    choix_tir(tirsx,tirsy,grille_user,changetirmode,countermodetir)
    if grille_user[y][x] == " " :
        grille_user[y][x] = "o"
        target_hit = False
        changetirmode=False
        countermodetir+=1
    elif grille_user[y][x] == "x" or grille_user[y][x] == "o" :
        target_hit = True
        changetirmode=False
        countermodetir+=1
    else:
        grille_user[y][x] = "x"
        target_hit = True
        changetirmode=True
        countermodetir=0
    return target_hit,changetirmode,countermodetir
def choix_tir (tirsx,tirsy,grille_user,changetirmode,countermodetir):
    """
    Choix de l'emplacement du tir de l'IA

    Paramètres:
    tirsx(liste)= Recense les coordonnées du tir de l'IA
    tirsy(liste)= Recense les coordonnées du tir de l'IA
    grille_user (liste)= grille de jeu de l'utilisateur
    changetirmode (bool)= Change le mode de tir de l'IA
    countermodetir (int)= Compte le nombre de tir depuis la dernière touche

    Retourne:
    x (int)=Coordonnée du tir de l'IA
    y (int)=Coordonnée du tir de l'IA
    """
    if changetirmode==True:
        x=tirsx[(len(tirsx)-1)-countermodetir]+randint(-1,1)
        y=tirsy[(len(tirsy)-1)-countermodetir]+randint(-1,1)
    else:
        x=randint(1,10)
        y=randint(1,10)
    return x,y

def player_turn(target_hit, turn_num):
    """
    Détermine à qui est ce que c'est de jouer

    Paramètres:
    target_hit (bool) : Indique si la cible sélectionnée a été touchée ou non.
    turn_num (int) : Numéro du tour actuel.
    Retour:
    turn (str) : Indique à qui est ce que c'est de jouer.
    turn_num (int) : Numéro du tour actuel.
    """
    if target_hit == False:
        turn_num += 1
    if (turn_num)%2 == 0:
        turn = "user"
        return turn,turn_num
    else:
        turn = "ai"
        return turn,turn_num


jouer=True
while jouer==True :#Programme principal
    tirsx=[]
    tirsy=[]
    countermodetir=0
    changetirmode=False
    turn_num=0
    name_bot()
    grille_ai=grille_vide_ai()
    affichage_ai(grille_ai)
    grille_user=grille_vide_user()
    print()
    affichage_user(grille_user)
    grille_ai_shown=grille_vide_ai_shown()
    quota=[1,1,2,1]
    quota_ai=[1,1,2,1]
    while quota!=[0,0,0,0]:
        how,nb_cases,y,x= placer_bateau(quota)
        grille_user,erreur_placement_boat_2=bateau(nb_cases,x,y,how,grille_user)
        quota=moins_quota(quota,erreur_placement_boat_2)
        affichage_ai(grille_ai_shown)
        affichage_user(grille_user)
    while quota_ai!=[0,0,0,0]:
        how,nb_cases,y,x=placer_bateau_ai(quota_ai)
        grille_ai,erreur_placement_boat_ai_2=bateau_ai(nb_cases, x, y, how,grille_ai)
        quota_ai=moins_quota_ai(quota_ai,erreur_placement_boat_ai_2)
    fin=False
    turn="user"
    victoire_a=False
    affichage_ai_test(grille_ai)
    while fin == False:
        if turn == "user":
            target_hit= target_hit_on_ai(grille_ai)
            turn, turn_num = player_turn(target_hit, turn_num)
        else:
            target_hit,changetirmode,countermodetir= target_hit_on_user(grille_user,x,y,changetirmode,countermodetir)
            turn, turn_num = player_turn(target_hit, turn_num)
        affichage_ai(grille_ai_shown)
        affichage_user(grille_user)
        victoire_a,fin = victoire(grille_ai, grille_user)
    print("Jeu terminé")
    if victoire_a==True:
        askplay=input("Rejouer ?")
        if askplay=="oui":
            jouer=True
        else:
            jouer=False
    elif victoire_a==False:
        askplay=input("Réessayer ?")
        if askplay=="oui":
            jouer=True
        else:
            jouer=False
